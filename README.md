# docker simple

Squelette docker prêt pour coder.
ça emporte Mysql, phpMyAdmin, et un mailer + php 7.4 prêt à l'emploi

## Lancer avec du code
 - Installer le code dans un dossier ou à la racine
 - Modifier le docker-compose.yml
 	- Modifier le conteneur www afin de préciser où se trouve le code :
 		Exemple si le code se situe au même niveau que le docker-compose.yml :
 			- ./:/var/www
 - Modifier le vhosts qui se trouve dans le dossier php
 	- Sert à modifier le point d'entrer du site (dans le cas de symfony ce sera dans le dossier public)
 	
## Lancer le projet
 - Se placer dans un terminal dans le dossier où se trouve le docker-compose.yml
 	- Lancer la commande : docker-compose up -d
 	
## Précision
 - Si on souhaite lancer des commandes php (ou des commandes symfony exemple : php bin/console ...)
 	- il faut se placer dans le conteneur :
 		- docker exec -ti [nom-du-conteneur] [shell-à-utiliser]
 			- Précision :
 				- nom du conteneur : dans le docker-compose.yml
 					- container_name
				- Shell à utiliser : Shell installé sur docker
					- Basic : /bin/bash
 			- Exemple :
 				- docker exec -ti www_docker /bin/bash
 				
## Connexions
 - BDD : 
	- DATABASE_URL=mysql://root:@db_docker_symfony:3306/db_name?serverVersion=5.7
	- exemple : 
		- DATABASE_URL="mysql://root@db:3306/nom_de_bdd"
			- db est le nom du server spécifié dans le docker-compose.yml
 - Mailer :
 	- MAILER_DSN=smtp://maildev_docker_symfony:25
 	
## Visualisation :
	PHPMyAdmin :
		- http://localhost:8080/
			Login :
				- user : root
				- mdp : pas de mot de passe
				
	Mailer :
		- http://localhost:8081/#/
		
## Prise en main
 - Pour pouvoir utiliser ce projet sur un autre repository il faut enlever le dossier .git ou déplacer le dossier php et le fichier docker-compose.yml dans le dossier du repository choisi
